﻿using System;

namespace QIDQuery.Objects
{
    [Serializable]
    public class Ayarlar
    {
        public string TCKimlikNo { get; set; }
        public string Sifre { get; set; }
    }
}
