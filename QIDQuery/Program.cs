﻿using QIDQuery.Forms;
using QIDQuery.Objects;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Windows.Forms;

namespace QIDQuery
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            InitAyarlar();
            Application.Run(new frmSorgu());

        }

        private static void InitAyarlar()
        {
            Globals.ExePath = Globals.GetExecutingDirectory().FullName;
            Globals.SettingsPath = Path.Combine(Globals.ExePath, "QIDQueryAyarlar.bin");
            if (File.Exists(Globals.SettingsPath))
            {
                try
                {
                    using (FileStream stream = new FileStream(Globals.SettingsPath, FileMode.Open, FileAccess.Read))
                    {
                        IFormatter formatter = new BinaryFormatter();
                        Globals.Ayarlar = (Ayarlar)formatter.Deserialize(stream);
                    }
                    BasicHttpBinding binding = new BasicHttpBinding();
                    EndpointAddress address = new EndpointAddress("http://smmmservis.tnb.org.tr/NPSKimlikDogrulamaServisi/Service.asmx");
                    KimlikServisi.ServiceSoapClient client = new KimlikServisi.ServiceSoapClient(binding, address);
                    KimlikServisi.ServisHataHeader hataHeader = client.BaglantiTesti(out DateTime zaman);
                    if (zaman == null || hataHeader != null)
                    {
                        MessageBox.Show("Webservis bağlantısı sağlanamadı. Lütfen daha sonra tekrar deneyiniz.");
                        Environment.Exit(0);
                    }

                    client.DisKullaniciKimlikDogrula2(
                        new KimlikServisi.DisKullaniciKimlik()
                        {
                            DisKullaniciTipi = KimlikServisi.DisKullaniciTipi.MaliMusavir,
                            ID = 0,
                            KayitDurumu = KimlikServisi.DataRowState.Detached,
                            KimlikNO = Globals.Decrypt(Globals.Ayarlar.TCKimlikNo),
                            KimlikNOTipi = KimlikServisi.KimlikNOTipi.TCKN,
                            NoterlikKodu = null,
                            NoterlikKullaniciAdi = null,
                            ProgramAdi = KimlikServisi.ProgramAdi.Belirtilmemis,
                            Sifre = Globals.Decrypt(Globals.Ayarlar.Sifre)
                        },
                    KimlikServisi.NPSIslemTipi.DefterOnayi,
                    new KimlikServisi.Zaman()
                    {
                        Yil = DateTime.Now.Year,
                        Ay = DateTime.Now.Month,
                        Gun = DateTime.Now.Day,
                        Saat = DateTime.Now.Hour,
                        Dakika = DateTime.Now.Minute,
                        Saniye = DateTime.Now.Second
                    }
                        , out long token);

                    if (token < 0)
                    {
                        MessageBox.Show("Kimlik doğrulaması başarılı olmadı lütfen kimlik bilgilerinizi kontrol ediniz.");
                        AyarYap();
                    }
                    else
                        Globals.Token = token;
                }
                catch (Exception)
                {
                    MessageBox.Show("Ayar dosyası uygun şekilde değil. Lütfen ayarları tekrar yapın.");
                    AyarYap();
                }
            }
            else
            {
                MessageBox.Show("Ayar dosyası uygun şekilde değil. Lütfen ayarları tekrar yapın.");
                AyarYap();
            }
        }

        public static void AyarYap()
        {
            using (frmAyarlar frm = new frmAyarlar())
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    Environment.Exit(0);
                }
                InitAyarlar();
            }
        }
    }
}
