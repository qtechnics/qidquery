﻿namespace QIDQuery
{
    partial class frmSorgu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSorgu));
            this.rbGercek = new System.Windows.Forms.RadioButton();
            this.rbTuzel = new System.Windows.Forms.RadioButton();
            this.pgSonuc = new System.Windows.Forms.PropertyGrid();
            this.txtSorgu = new System.Windows.Forms.TextBox();
            this.btnSorgula = new System.Windows.Forms.Button();
            this.btnKapat = new System.Windows.Forms.Button();
            this.btnAyarlar = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnEFatura = new System.Windows.Forms.Button();
            this.btnKapat2 = new System.Windows.Forms.Button();
            this.txtVergiNo = new System.Windows.Forms.TextBox();
            this.txtSonucVergiNo = new System.Windows.Forms.TextBox();
            this.txtSonucTur = new System.Windows.Forms.TextBox();
            this.txtSonucUnvan = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbGercek
            // 
            this.rbGercek.AutoSize = true;
            this.rbGercek.Checked = true;
            this.rbGercek.Location = new System.Drawing.Point(17, 16);
            this.rbGercek.Name = "rbGercek";
            this.rbGercek.Size = new System.Drawing.Size(79, 17);
            this.rbGercek.TabIndex = 0;
            this.rbGercek.TabStop = true;
            this.rbGercek.Text = "Gerçek Kişi";
            this.rbGercek.UseVisualStyleBackColor = true;
            // 
            // rbTuzel
            // 
            this.rbTuzel.AutoSize = true;
            this.rbTuzel.Location = new System.Drawing.Point(17, 39);
            this.rbTuzel.Name = "rbTuzel";
            this.rbTuzel.Size = new System.Drawing.Size(70, 17);
            this.rbTuzel.TabIndex = 1;
            this.rbTuzel.Text = "Tüzel Kişi";
            this.rbTuzel.UseVisualStyleBackColor = true;
            // 
            // pgSonuc
            // 
            this.pgSonuc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgSonuc.HelpVisible = false;
            this.pgSonuc.Location = new System.Drawing.Point(17, 74);
            this.pgSonuc.Name = "pgSonuc";
            this.pgSonuc.Size = new System.Drawing.Size(442, 573);
            this.pgSonuc.TabIndex = 6;
            this.pgSonuc.ToolbarVisible = false;
            // 
            // txtSorgu
            // 
            this.txtSorgu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSorgu.Location = new System.Drawing.Point(113, 16);
            this.txtSorgu.Name = "txtSorgu";
            this.txtSorgu.Size = new System.Drawing.Size(184, 20);
            this.txtSorgu.TabIndex = 2;
            this.txtSorgu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtSorgu_KeyDown);
            // 
            // btnSorgula
            // 
            this.btnSorgula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSorgula.Location = new System.Drawing.Point(303, 16);
            this.btnSorgula.Name = "btnSorgula";
            this.btnSorgula.Size = new System.Drawing.Size(75, 23);
            this.btnSorgula.TabIndex = 3;
            this.btnSorgula.Text = "Sorgula";
            this.btnSorgula.UseVisualStyleBackColor = true;
            this.btnSorgula.Click += new System.EventHandler(this.btnSorgula_Click);
            // 
            // btnKapat
            // 
            this.btnKapat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKapat.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnKapat.Location = new System.Drawing.Point(384, 16);
            this.btnKapat.Name = "btnKapat";
            this.btnKapat.Size = new System.Drawing.Size(75, 23);
            this.btnKapat.TabIndex = 4;
            this.btnKapat.Text = "Kapat";
            this.btnKapat.UseVisualStyleBackColor = true;
            this.btnKapat.Click += new System.EventHandler(this.btnKapat_Click);
            // 
            // btnAyarlar
            // 
            this.btnAyarlar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAyarlar.Location = new System.Drawing.Point(384, 45);
            this.btnAyarlar.Name = "btnAyarlar";
            this.btnAyarlar.Size = new System.Drawing.Size(75, 23);
            this.btnAyarlar.TabIndex = 5;
            this.btnAyarlar.Text = "Ayarlar";
            this.btnAyarlar.UseVisualStyleBackColor = true;
            this.btnAyarlar.Click += new System.EventHandler(this.btnAyarlar_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(473, 679);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.rbGercek);
            this.tabPage1.Controls.Add(this.btnAyarlar);
            this.tabPage1.Controls.Add(this.rbTuzel);
            this.tabPage1.Controls.Add(this.btnKapat);
            this.tabPage1.Controls.Add(this.pgSonuc);
            this.tabPage1.Controls.Add(this.btnSorgula);
            this.tabPage1.Controls.Add(this.txtSorgu);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(465, 653);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Bilgi Sorgulama";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.btnEFatura);
            this.tabPage2.Controls.Add(this.btnKapat2);
            this.tabPage2.Controls.Add(this.txtSonucUnvan);
            this.tabPage2.Controls.Add(this.txtSonucTur);
            this.tabPage2.Controls.Add(this.txtSonucVergiNo);
            this.tabPage2.Controls.Add(this.txtVergiNo);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(465, 653);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "E-Fatura Mükellefi";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnEFatura
            // 
            this.btnEFatura.Location = new System.Drawing.Point(303, 13);
            this.btnEFatura.Name = "btnEFatura";
            this.btnEFatura.Size = new System.Drawing.Size(75, 23);
            this.btnEFatura.TabIndex = 1;
            this.btnEFatura.Text = "Sorgula";
            this.btnEFatura.UseVisualStyleBackColor = true;
            this.btnEFatura.Click += new System.EventHandler(this.BtnEFatura_Click);
            // 
            // btnKapat2
            // 
            this.btnKapat2.Location = new System.Drawing.Point(384, 13);
            this.btnKapat2.Name = "btnKapat2";
            this.btnKapat2.Size = new System.Drawing.Size(75, 23);
            this.btnKapat2.TabIndex = 1;
            this.btnKapat2.Text = "Kapat";
            this.btnKapat2.UseVisualStyleBackColor = true;
            this.btnKapat2.Click += new System.EventHandler(this.BtnKapat2_Click);
            // 
            // txtVergiNo
            // 
            this.txtVergiNo.Location = new System.Drawing.Point(60, 15);
            this.txtVergiNo.MaxLength = 10;
            this.txtVergiNo.Name = "txtVergiNo";
            this.txtVergiNo.Size = new System.Drawing.Size(223, 20);
            this.txtVergiNo.TabIndex = 0;
            this.txtVergiNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtVergiNo_KeyDown);
            // 
            // txtSonucVergiNo
            // 
            this.txtSonucVergiNo.Location = new System.Drawing.Point(60, 114);
            this.txtSonucVergiNo.MaxLength = 10;
            this.txtSonucVergiNo.Name = "txtSonucVergiNo";
            this.txtSonucVergiNo.Size = new System.Drawing.Size(399, 20);
            this.txtSonucVergiNo.TabIndex = 0;
            // 
            // txtSonucTur
            // 
            this.txtSonucTur.Location = new System.Drawing.Point(60, 140);
            this.txtSonucTur.MaxLength = 10;
            this.txtSonucTur.Name = "txtSonucTur";
            this.txtSonucTur.Size = new System.Drawing.Size(399, 20);
            this.txtSonucTur.TabIndex = 0;
            // 
            // txtSonucUnvan
            // 
            this.txtSonucUnvan.Location = new System.Drawing.Point(60, 166);
            this.txtSonucUnvan.MaxLength = 10;
            this.txtSonucUnvan.Name = "txtSonucUnvan";
            this.txtSonucUnvan.Size = new System.Drawing.Size(399, 20);
            this.txtSonucUnvan.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vergi No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Vergi No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tür";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Ünvan";
            // 
            // frmSorgu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnKapat;
            this.ClientSize = new System.Drawing.Size(491, 698);
            this.Controls.Add(this.tabControl1);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSorgu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QIDQuery - Sorgulama";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmSorgu_HelpButtonClicked);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rbGercek;
        private System.Windows.Forms.RadioButton rbTuzel;
        private System.Windows.Forms.PropertyGrid pgSonuc;
        private System.Windows.Forms.TextBox txtSorgu;
        private System.Windows.Forms.Button btnSorgula;
        private System.Windows.Forms.Button btnKapat;
        private System.Windows.Forms.Button btnAyarlar;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnEFatura;
        private System.Windows.Forms.Button btnKapat2;
        private System.Windows.Forms.TextBox txtVergiNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSonucUnvan;
        private System.Windows.Forms.TextBox txtSonucTur;
        private System.Windows.Forms.TextBox txtSonucVergiNo;
    }
}

