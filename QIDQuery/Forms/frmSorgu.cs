﻿using QIDQuery.GIBBilgiServisi;
using RestSharp;
using System;
using System.ServiceModel;
using System.Windows.Forms;

namespace QIDQuery
{
    public partial class frmSorgu : Form
    {
        public frmSorgu() => InitializeComponent();

        private void btnAyarlar_Click(object sender, EventArgs e) => Program.AyarYap();

        private void btnSorgula_Click(object sender, EventArgs e)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            EndpointAddress address = new EndpointAddress("http://smmmservis.tnb.org.tr/GIBBilgiServisi/GBS.asmx");
            using (GIBBilgiServisiSoapClient client = new GIBBilgiServisiSoapClient(binding, address))
            {
                if (string.IsNullOrEmpty(txtSorgu.Text))
                    return;
                if (!long.TryParse(txtSorgu.Text, out long numara) && rbGercek.Checked)
                {
                    MessageBox.Show("Girmiş olduğunuz veri hatalı. Lütfen kontrol edin");
                    return;
                }

                ServisHataHeader hataHeader = client.BaglantiTesti(out DateTime zaman);
                if (zaman == null || hataHeader != null)
                {
                    MessageBox.Show("Webservis bağlantısı sağlanamadı. Lütfen daha sonra tekrar deneyiniz.");
                    Environment.Exit(0);
                }

                MerkezBilgiSorguSonuc sonuc;

                if (rbGercek.Checked)
                {
                    client.GercekSahisMukellefMerkezBilgiSorgu(new BilgiServisHeader()
                    {
                        IslemTipi = NPSIslemTipi.DefterOnayi,
                        NPSBelgeNO = Globals.Token,
                        Program = ProgramAdi.Belirtilmemis,
                        Sifre = null
                    }, null, numara, out sonuc);
                }
                else
                {
                    client.TuzelSahisMukellefMerkezBilgiSorgu(new BilgiServisHeader()
                    {
                        IslemTipi = NPSIslemTipi.DefterOnayi,
                        NPSBelgeNO = Globals.Token,
                        Program = ProgramAdi.Belirtilmemis,
                        Sifre = null
                    }, null, txtSorgu.Text, out sonuc);
                }

                pgSonuc.SelectedObject = sonuc;
            }
        }

        private void btnKapat_Click(object sender, EventArgs e) => Close();

        private void frmSorgu_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e) => System.Diagnostics.Process.Start("https://www.qtechnics.com");

        private void BtnEFatura_Click(object sender, EventArgs e)
        {
            if (txtVergiNo.Text.Length != 10 && !long.TryParse(txtVergiNo.Text, out long vergiNo))
            {
                MessageBox.Show("Girmiş olduğunuz veri hatalı. Lütfen kontrol edin");
                return;
            }

            try
            {
                var client = new RestClient("http://sorgu.efatura.gov.tr/kullanicilar/xliste.php");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Length", "35");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", "sorgu.efatura.gov.tr");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Pragma", "no-cache");
                request.AddHeader("Referer", "http://sorgu.efatura.gov.tr/kullanicilar/xliste.php");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Accept-Language", "tr-TR,tr;q=0.8,en-US;q=0.5,en;q=0.3");
                request.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0");
                request.AddParameter("undefined", $"search_string={txtVergiNo.Text}&submit=Ara", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                    document.LoadHtml(response.Content);
                    var kurumlar = document.DocumentNode.SelectNodes("//table[contains(@class, 'kurumlar')]");

                    if (kurumlar != null && kurumlar.Count >= 1)
                    {
                        var trs = kurumlar[0].SelectNodes("//tr");
                        if (trs.Count > 2)
                        {
                            txtSonucVergiNo.Text = trs[1].ChildNodes[0].InnerText;
                            txtSonucTur.Text = trs[1].ChildNodes[1].InnerText;
                            txtSonucUnvan.Text = trs[1].ChildNodes[2].InnerText;
                        }
                        else
                            throw new Exception("Sonuç Bulunamadı.");

                    }
                    else
                        throw new Exception("Sonuç Bulunamadı.");
                }
                else
                    throw new Exception("Sorguda hata oluştu.");
            }
            catch (Exception ex)
            {
                txtSonucVergiNo.Text = txtSonucTur.Text = txtSonucUnvan.Text = ex.Message;
            }
        }

        private void BtnKapat2_Click(object sender, EventArgs e) => Close();

        private void TxtVergiNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnEFatura.PerformClick();
                e.Handled = true;
            }
        }

        private void TxtSorgu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSorgula.PerformClick();
                e.Handled = true;
            }
        }
    }
}
