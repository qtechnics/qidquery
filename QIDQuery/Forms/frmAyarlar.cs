﻿using QIDQuery.Objects;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace QIDQuery.Forms
{
    public partial class frmAyarlar : Form
    {
        public frmAyarlar()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            if (Globals.Ayarlar != null)
            {
                txtUsername.Text = Globals.Decrypt(Globals.Ayarlar.TCKimlikNo);
                txtPassword.Text = Globals.Decrypt(Globals.Ayarlar.Sifre);
            }
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            using (FileStream stream = new FileStream(Globals.SettingsPath, FileMode.Create, FileAccess.Write))
            {
                IFormatter formatter = new BinaryFormatter();
                Ayarlar ayarlar = new Ayarlar() { TCKimlikNo = Globals.Encrypt(txtUsername.Text), Sifre = Globals.Encrypt(txtPassword.Text) };
                formatter.Serialize(stream, ayarlar);
                stream.Close();
            }
            DialogResult = DialogResult.OK;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://smmm.tnb.org.tr/GIB/MUHASEBECI/MuhasebeciGiris.aspx");
        }

        private void frmAyarlar_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.qtechnics.com");
        }
    }
}
